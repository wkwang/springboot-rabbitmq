# springboot-rabbitmq

#### 介绍
这是一个rabbitmq的学习demo，使用的是**TopicExchange**，有生产者和消费者，实现了手动ACK确认机制。

#### 安装
参考我的笔记进行安装和配置
http://note.youdao.com/noteshare?id=791be70fdb038a253a81cbbe32114ed1&sub=WEB8bde5883650aaa6c0727c3d20d011b77

#### 调试
我是在windows上安装的rabbitmq，故需要启动%RABBIT_HOME%/sbin/rabbitmq-server.bat  
启动后，依次运行producer/startup和consumer/startup即可。  
然后查看管理界面中的各项功能。

