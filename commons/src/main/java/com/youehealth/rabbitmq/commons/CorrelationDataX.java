package com.youehealth.rabbitmq.commons;

import org.springframework.amqp.rabbit.support.CorrelationData;

/**
 * Created by Administrator on 2019/01/10.
 */
public class CorrelationDataX extends CorrelationData {
    private final String exchange;
    private final String routingKey;
    private int retryCount = 0;
    private final Object message;

    public Object getMessage() {
        return message;
    }

    public String getExchange() {
        return exchange;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public int getRetryCount() {
        return retryCount;
    }
    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    private CorrelationDataX(Builder builder) {
        this.setId(builder.id);
        this.message = builder.message;
        this.exchange = builder.exchange;
        this.routingKey = builder.routingKey;
    }

    public static class Builder {
        private String id;
        private String exchange;
        private String routingKey;
        private volatile Object message;

        public Builder id(String messageId) {
            this.id = messageId;
            return this;
        }

        public Builder message(Object message) {
            this.message = message;
            return this;
        }

        public Builder exchange(String exchange) {
            this.exchange = exchange;
            return this;
        }

        public Builder routingKey(String routingKey) {
            this.routingKey = routingKey;
            return this;
        }

        public CorrelationDataX build() {
            return new CorrelationDataX(this);
        }
    }
}
