package com.youehealth.rabbitmq.commons;

import java.time.LocalDate;

/**
 * Created by Administrator on 2018/12/28.
 */
public class PatientPayload implements Payload {
    private static final long serialVersionUID = -7092313054537964121L;

    private final long id;
    private final String name;
    private final String gender;
    private final LocalDate birthday;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    private PatientPayload(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.gender = builder.gender;
        this.birthday = builder.birthday;
    }

    public static class Builder {

        private long id;
        private String name;
        private String gender;
        private LocalDate birthday;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder birthday(LocalDate birthday) {
            this.birthday = birthday;
            return this;
        }

        public PatientPayload build() {
            return new PatientPayload(this);
        }
    }
}
