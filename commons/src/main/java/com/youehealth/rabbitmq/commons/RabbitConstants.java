package com.youehealth.rabbitmq.commons;

/**
 * Created by Administrator on 2019/01/10.
 */
public class RabbitConstants {
    public static final String EXCHANGE_NAME = "exchange.test";

    public static final String QUEUE_NAME_TEST1 = "queue.test1";

    public static final String QUEUE_NAME_TEST2 = "queue.test2";

    public static final String QUEUE_NAME_TEST3 = "queue_any.test";

    public static final String ROUTINGKEY_FOR_BINDING1 = "queue.test1";

    public static final String ROUTINGKEY_FOR_BINDING2 = "queue.*";

    public static final String ROUTINGKEY_FOR_BINDING3 = "*.test";
}
