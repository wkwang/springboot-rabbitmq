package com.youehealth.rabbitmq.consumer;

import com.youehealth.rabbitmq.commons.Payload;
import org.springframework.amqp.core.Message;

/**
 * 函数式接口，用于自定义处理异常消息，如保存到缓存或数据库中
 * Created by Administrator on 2019/01/10.
 */
@FunctionalInterface
public interface ErrorMessageHandler<P extends Payload, M extends Message> {
    void accept(P payload, M message);
}
