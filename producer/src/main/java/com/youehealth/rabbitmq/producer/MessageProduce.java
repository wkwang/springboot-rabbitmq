package com.youehealth.rabbitmq.producer;

import com.youehealth.rabbitmq.commons.Payload;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.InitializingBean;

/**
 * Created by Administrator on 2019/01/01.
 */
public interface MessageProduce extends RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback, InitializingBean {

    void sendMessage(String routingKey, Payload payload);
}
