package com.youehealth.rabbitmq.producer;

import com.youehealth.rabbitmq.commons.RabbitConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Administrator on 2018/12/28.
 */
@Configuration
public class ProducerConfig {

    @Bean
    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

    @Bean
    public Queue testQueue1(RabbitAdmin rabbitAdmin) {
        Queue queue = new Queue(RabbitConstants.QUEUE_NAME_TEST1, true);
        rabbitAdmin.declareQueue(queue);
        return queue;
    }

    @Bean
    public Queue testQueue2(RabbitAdmin rabbitAdmin) {
        Queue queue = new Queue(RabbitConstants.QUEUE_NAME_TEST2, true);
        rabbitAdmin.declareQueue(queue);
        return queue;
    }

    @Bean
    public Queue testQueue3(RabbitAdmin rabbitAdmin) {
        Queue queue = new Queue(RabbitConstants.QUEUE_NAME_TEST3, true);
        rabbitAdmin.declareQueue(queue);
        return queue;
    }

    @Bean
    public TopicExchange topicExchange(RabbitAdmin rabbitAdmin) {
        TopicExchange exchange = new TopicExchange(RabbitConstants.EXCHANGE_NAME);
        rabbitAdmin.declareExchange(exchange);
        return exchange;
    }

    @Bean
    public Binding testBinding1(Queue testQueue1, TopicExchange topicExchange, RabbitAdmin rabbitAdmin) {
        //binding queue.test1
        Binding binding = BindingBuilder.bind(testQueue1).to(topicExchange)
                .with(RabbitConstants.ROUTINGKEY_FOR_BINDING1);
        rabbitAdmin.declareBinding(binding);
        return binding;
    }

    @Bean
    public Binding testBinding2(Queue testQueue2, TopicExchange topicExchange, RabbitAdmin rabbitAdmin) {
        //binding queue.test1,queue.test2
        Binding binding = BindingBuilder.bind(testQueue2).to(topicExchange)
                .with(RabbitConstants.ROUTINGKEY_FOR_BINDING2);
        rabbitAdmin.declareBinding(binding);
        return binding;
    }

    @Bean
    public Binding testBinding3(Queue testQueue3, TopicExchange topicExchange, RabbitAdmin rabbitAdmin) {
        //binding queue_any.test
        Binding binding = BindingBuilder.bind(testQueue3).to(topicExchange)
                .with(RabbitConstants.ROUTINGKEY_FOR_BINDING3);
        rabbitAdmin.declareBinding(binding);
        return binding;
    }
}
