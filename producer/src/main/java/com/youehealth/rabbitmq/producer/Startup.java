package com.youehealth.rabbitmq.producer;

import com.youehealth.rabbitmq.commons.PatientPayload;
import com.youehealth.rabbitmq.commons.Payload;
import com.youehealth.rabbitmq.commons.RabbitConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;

/**
 * Created by Administrator on 2018/12/28.
 */
@SpringBootApplication
public class Startup implements CommandLineRunner {

    @Autowired
    private BasicMessageProduce producer;

    @Override
    public void run(String... args) throws Exception {
        Payload payload1 = new PatientPayload.Builder()
                .id(1)
                .name("zhang san")
                .gender("male")
                .birthday(LocalDate.of(1965, 2, 3))
                .build();
        producer.sendMessage(RabbitConstants.ROUTINGKEY_FOR_BINDING1, payload1);
        Payload payload2 = new PatientPayload.Builder()
                .id(2)
                .name("li si")
                .gender("female")
                .birthday(LocalDate.of(1985, 12, 23))
                .build();
        producer.sendMessage(RabbitConstants.ROUTINGKEY_FOR_BINDING2, payload2);
        Payload payload3 = new PatientPayload.Builder()
                .id(1)
                .name("wang wu")
                .gender("female")
                .birthday(LocalDate.of(1972, 5, 13))
                .build();
        producer.sendMessage(RabbitConstants.ROUTINGKEY_FOR_BINDING3, payload3);
        System.out.println("生产者已发送完...");
    }

    public static void main(String[] args) {
        SpringApplication.run(Startup.class, args);
    }
}
